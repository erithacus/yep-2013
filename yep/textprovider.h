#ifndef TEXTPROVIDER_H
#define TEXTPROVIDER_H

#include "mathhelpers.h"

class Character
{
  public: 
    Character(Point** pPoints, short sizeP);
    Point* getPoint(short index) const;
    short getNumberOfPoints() const;
    short getDimensionX() const ;
    short getDimensionY() const;
    void setBlank(bool b, short length);
    bool isBlank() const;
    
  private:
    bool m_bBlank;
    short m_iSize;
    short m_iBoundX;
    short m_iBoundY;
    Point** m_arrayPoints;
};

extern Character* asciiTable[128];

class TextProvider
{
public:
  TextProvider();
  Character* getDrawingForChar(const char c) const;
  
private:
  Character* asciiTable[128];
};

#endif //TEXTPROVIDER_H
