#include "mathhelpers.h"

Point::Point(short x, short y)
{
  m_iX = x;
  m_iY = y;
}

Point::Point() : Point(0,0)
{ }

short Point::getX() const
{
  return m_iX;
}

short Point::getY() const
{
  return m_iY;
}

short Point::setX(const short x)
{
  m_iX = x;
}

short Point::setY(const short y)
{
  m_iY = y;
}

Point Point::addPoint(const Point p) const
{
  return Point(getX() + p.getX(), getY() + p.getY());
}

Point Point::addPoint(const short x, const short y) const
{
  Point p(x,y);
  return addPoint(p);
}

bool Point::equals(Point p) const
{
  return ((getX() == p.getX()) && (getY() == p.getY()));
}
