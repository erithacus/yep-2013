#ifndef MATRIXCONTROLLER_H
#define MATRIXCONTROLLER_H

#include "drawinghelpers.h"
#include "textprovider.h"

class Matrix;

class MatrixController
{
public:
  MatrixController();
  ~MatrixController();
  void setMatrix(Matrix* pMatrix);
  
  short getHeightOfMatrix() const;
  short getWidthOfMatrix() const;
  
  void show();
  
  Point drawString(const char text[], const Point pos, const Colour colour);
  Point drawString(const char text[], const Point pos);
  Point eraseString(const char text[], const Point pos);

  Point drawCharacter(const Character* character, const Point pos, const Colour colour);
  Point drawCharacter(const Character* character, const Point pos);
  Point eraseCharacter(const Character* character, const Point pos);
  
  void drawSquare(const Point pos, const short width, const short height, const Colour colour);
  void drawSquare(const Point pos, const short width, const short height);
  void eraseSquare(const Point pos, const short width, const short height);
  
  void drawSquareFilled(const Point pos, const short width, const short height, const Colour colour);
  void drawSquareFilled(const Point pos, const short width, const short height);
  void eraseSquareFilled(const Point pos, const short width, const short height);
  
  void drawPoint(const Point p, const Colour colour);
  void drawPoint(const Point p);
  void erasePoint(const Point p);
  
  void drawLine(const Point p1, const Point p2, const Colour colour);
  void drawLine(const Point p1, const Point p2);
  void eraseLine(const Point p1, const Point p2);
  
  void drawCircle(const Point pos, const short radius, const Colour colour);
  void drawCircle(const Point pos, const short radius);
  void eraseCircle(const Point pos, const short radius);
  
  void drawImage(const Image* img, const Point pos);
  void eraseImage(const Image* img, const Point pos);
  
  void setBackgroundColour(const Colour c);
  void setBackgroundColour(const short r, const short g, const short b);
  void setDrawingColour(const Colour c);
  void setDrawingColour(const short r, const short g, const short b);
  Colour getBackgroundColour() const;
  Colour getDrawingColour() const;
  
  
private:
  Matrix* m_pMatrix;
  TextProvider m_textProvider;
};

#endif //MATRIXCONTROLLER_H
