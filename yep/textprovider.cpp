#include "textprovider.h"

Character::Character(Point** pPoints, short sizeP)
{
  m_arrayPoints = pPoints;
  m_iSize = sizeP;
  short tempX=0;
  short tempY=0;
  for (int i=0; i < m_iSize ;i++)
  {
    Point* p = m_arrayPoints[i];
    if (p->getX() > tempX) 
      tempX = p->getX();
    if (p->getY() > tempY) 
      tempY = p->getY();
  }
  m_iBoundX = tempX;
  m_iBoundY = tempY;
  setBlank(false, getDimensionX());
}

void Character::setBlank(bool b, short length)
{
  m_bBlank = b;
  m_iBoundX = length;
}

bool Character::isBlank() const
{
  return m_bBlank;
}

short Character::getNumberOfPoints() const
{
  return m_iSize;
}

short Character::getDimensionX() const
{
  return m_iBoundX;
}

short Character::getDimensionY() const
{
  return m_iBoundY;
}

Point* Character::getPoint(short index) const
{
  if (index < getNumberOfPoints())
    return m_arrayPoints[index];
  else
    return m_arrayPoints[0];
}

//needed points for the characters
Point point_0_0(0,0);
Point point_0_1(0,1);
Point point_0_2(0,2);
Point point_0_3(0,3);
Point point_0_4(0,4);
Point point_1_0(1,0);
Point point_1_1(1,1);
Point point_1_2(1,2);
Point point_1_3(1,3);
Point point_1_4(1,4);
Point point_2_0(2,0);
Point point_2_1(2,1);
Point point_2_2(2,2);
Point point_2_3(2,3);
Point point_2_4(2,4);
Point point_3_0(3,0);
Point point_3_1(3,1);
Point point_3_2(3,2);
Point point_3_3(3,3);
Point point_3_4(3,4);
Point point_4_0(4,0);
Point point_4_1(4,1);
Point point_4_2(4,2);
Point point_4_3(4,3);
Point point_4_4(4,4);

Point* points_NULL[25] = {
  &point_0_0,&point_1_0,&point_2_0,&point_3_0,&point_4_0,
  &point_0_1,&point_1_1,&point_2_1,&point_3_1,&point_4_1,
  &point_0_2,&point_1_2,&point_2_2,&point_3_2,&point_4_2,
  &point_0_3,&point_1_3,&point_2_3,&point_3_3,&point_4_3,
  &point_0_4,&point_1_4,&point_2_4,&point_3_4,&point_4_4
};
Point* points_A[12] = {
             &point_1_0,&point_2_0,
  &point_0_1,                      &point_3_1,
  &point_0_2,&point_1_2,&point_2_2,&point_3_2,
  &point_0_3,                      &point_3_3,
  &point_0_4,                      &point_3_4
};
Point* points_B[13] = {
  &point_0_0,&point_1_0,&point_2_0,
  &point_0_1,                      &point_3_1,
  &point_0_2,&point_1_2,&point_2_2,
  &point_0_3,                      &point_3_3,
  &point_0_4,&point_1_4,&point_2_4,
};
Point* points_C[9] = {
             &point_1_0,&point_2_0,&point_3_0,
  &point_0_1,
  &point_0_2,
  &point_0_3,
             &point_1_4,&point_2_4,&point_3_4
};
Point* points_D[12] = {
  &point_0_0,&point_1_0,&point_2_0,
  &point_0_1,                      &point_3_1,
  &point_0_2,                      &point_3_2,
  &point_0_3,                      &point_3_3,
  &point_0_4,&point_1_4,&point_2_4
};
Point* points_E[10] = {
  &point_0_0,&point_1_0,&point_2_0,
  &point_0_1,
  &point_0_2,&point_1_2,
  &point_0_3,
  &point_0_4,&point_1_4,&point_2_4
};
Point* points_F[10] = {
  &point_0_0,&point_1_0,&point_2_0,&point_3_0,
  &point_0_1,
  &point_0_2,&point_1_2,&point_2_2,
  &point_0_3,
  &point_0_4
};
Point* points_G[14] = {
  &point_0_0,&point_1_0,&point_2_0,&point_3_0,
  &point_0_1,
  &point_0_2,           &point_2_2,&point_3_2,
  &point_0_3,                      &point_3_3,
  &point_0_4,&point_1_4,&point_2_4,&point_3_4
};
Point* points_H[12] = {
  &point_0_0,                      &point_3_0,
  &point_0_1,                      &point_3_1,
  &point_0_2,&point_1_2,&point_2_2,&point_3_2,
  &point_0_3,                      &point_3_3,
  &point_0_4,                      &point_3_4
};
Point* points_I[9] = {
  &point_0_0,&point_1_0,&point_2_0,
             &point_1_1,
             &point_1_2,
             &point_1_3,
  &point_0_4,&point_1_4,&point_2_4
};
Point* points_J[10] = {
             &point_1_0,&point_2_0,&point_3_0,
                        &point_2_1,
                        &point_2_2,
  &point_0_3,           &point_2_3,
  &point_0_4,&point_1_4,&point_2_4
};
Point* points_K[10] = {
  &point_0_0,                      &point_3_0,
  &point_0_1,           &point_2_1,
  &point_0_2,&point_1_2,
  &point_0_3,           &point_2_3,
  &point_0_4,                      &point_3_4
};
Point* points_L[7] = {
  &point_0_0,
  &point_0_1,
  &point_0_2,
  &point_0_3,
  &point_0_4,&point_1_4,&point_2_4
};
Point* points_M[13] = {
  &point_0_0,                                 &point_4_0,
  &point_0_1,&point_1_1,           &point_3_1,&point_4_1,
  &point_0_2,           &point_2_2,           &point_4_2,
  &point_0_3,                                 &point_4_3,
  &point_0_4,                                 &point_4_4
};
Point* points_N[13] = {
  &point_0_0,                                 &point_4_0,
  &point_0_1,&point_1_1,                      &point_4_1,
  &point_0_2,           &point_2_2,           &point_4_2,
  &point_0_3,                      &point_3_3,&point_4_3,
  &point_0_4,                                 &point_4_4
};
Point* points_O[16] = {
  &point_0_0,&point_1_0,&point_2_0,&point_3_0,
  &point_0_1,                      &point_3_1,
  &point_0_2,                      &point_3_2,
  &point_0_3,                      &point_3_3,
  &point_0_4,&point_1_4,&point_2_4,&point_3_4
};
Point* points_P[10] = {
  &point_0_0,&point_1_0,&point_2_0,
  &point_0_1,                      &point_3_1,
  &point_0_2,&point_1_2,&point_2_2,
  &point_0_3,
  &point_0_4
};
Point* points_Q[17] = {
  &point_0_0,&point_1_0,&point_2_0,&point_3_0,&point_4_0,
  &point_0_1,                                 &point_4_1,
  &point_0_2,                                 &point_4_2,
  &point_0_3,                      &point_3_3,&point_4_3,
  &point_0_4,&point_1_4,&point_2_4,&point_3_4,&point_4_4
};
Point* points_R[16] = {
  &point_0_0,&point_1_0,&point_2_0,&point_3_0,&point_4_0,
  &point_0_1,                                 &point_4_1,
  &point_0_2,&point_1_2,&point_2_2,&point_3_2,&point_4_2,
  &point_0_3,                      &point_3_3,
  &point_0_4,                                 &point_4_4
};
Point* points_S[14] = {
  &point_0_0,&point_1_0,&point_2_0,&point_3_0,
  &point_0_1,
  &point_0_2,&point_1_2,&point_2_2,&point_3_2,
                                   &point_3_3,
  &point_0_4,&point_1_4,&point_2_4,&point_3_4
};
Point* points_T[9] = {
  &point_0_0,&point_1_0,&point_2_0,&point_3_0,&point_4_0,
                        &point_2_1,
                        &point_2_2,
                        &point_2_3,
                        &point_2_4
};
Point* points_U[12] = {
  &point_0_0,                      &point_3_0,
  &point_0_1,                      &point_3_1,
  &point_0_2,                      &point_3_2,
  &point_0_3,                      &point_3_3,
  &point_0_4,&point_1_4,&point_2_4,&point_3_4
};
Point* points_V[9] = {
  &point_0_0,                                 &point_4_0,
  &point_0_1,                                 &point_4_1,
             &point_1_2,           &point_3_2,
             &point_1_3,           &point_3_3,
                        &point_2_4
};
Point* points_W[13] = {
  &point_0_0,                                 &point_4_0,
  &point_0_1,                                 &point_4_1,
  &point_0_2,           &point_2_2,           &point_4_2,
  &point_0_3,&point_1_3,           &point_3_3,&point_4_3,
  &point_0_4,                                 &point_4_4
};
Point* points_X[9] = {
  &point_0_0,                                 &point_4_0,
             &point_1_1,           &point_3_1,
                       &point_2_2,
             &point_1_3,            &point_3_3,
  &point_0_4,                                 &point_4_4
};
Point* points_Y[7] = {
  &point_0_0,                                 &point_4_0,
             &point_1_1,           &point_3_1,
                        &point_2_2,
                        &point_2_3,
                        &point_2_4
};
Point* points_Z[13] = {
  &point_0_0,&point_1_0,&point_2_0,&point_3_0,&point_4_0,
                                   &point_3_1,
                        &point_2_2,
             &point_1_3,
  &point_0_4,&point_1_4,&point_2_4,&point_3_4,&point_4_4
};
Point* points_0[12] = {
             &point_1_0,&point_2_0,&point_3_0,
  &point_0_1,                                 &point_4_1,
  &point_0_2,                                 &point_4_2,
  &point_0_3,                                 &point_4_3,
             &point_1_4,&point_2_4,&point_3_4
};
Point* points_1[6] = {
  &point_0_0,&point_1_0,
             &point_1_1,
             &point_1_2,
             &point_1_3,
             &point_1_4,
};
Point* points_2[10] = {
             &point_1_0,&point_2_0,
  &point_0_1,                      &point_3_1,
                        &point_2_2,
              &point_1_3,
  &point_0_4,&point_1_4,&point_2_4,&point_3_4
};
Point* points_3[13] = {
  &point_0_0,&point_1_0,&point_2_0,&point_3_0,
                                   &point_3_1,
             &point_1_2,&point_2_2,&point_3_2,
                                   &point_3_3,
  &point_0_4,&point_1_4,&point_2_4,&point_3_4
};
Point* points_4[10] = {
  &point_0_0,                      &point_3_0,
  &point_0_1,                      &point_3_1,
  &point_0_2,&point_1_2,&point_2_2,&point_3_2,
                                   &point_3_3,
                                   &point_3_4
};
Point* points_5[12] = {
  &point_0_0,&point_1_0,&point_2_0,&point_3_0,
  &point_0_1,
  &point_0_2,&point_1_2,&point_2_2,
                                   &point_3_3,
  &point_0_4,&point_1_4,&point_2_4
};
Point* points_6[15] = {
  &point_0_0,&point_1_0,&point_2_0,&point_3_0,
  &point_0_1,
  &point_0_2,&point_1_2,&point_2_2,&point_3_2,
  &point_0_3,                      &point_3_3,
  &point_0_4,&point_1_4,&point_2_4,&point_3_4
};
Point* points_7[8] = {
  &point_0_0,&point_1_0,&point_2_0,&point_3_0,
                                   &point_3_1,
                        &point_2_2,
             &point_1_3,
  &point_0_4
};
Point* points_8[14] = {
  &point_0_0,&point_1_0,&point_2_0,&point_3_0,
  &point_0_1,                      &point_3_1,
             &point_1_2,&point_2_2,
  &point_0_3,                      &point_3_3,
  &point_0_4,&point_1_4,&point_2_4,&point_3_4
};
Point* points_9[15] = {
  &point_0_0,&point_1_0,&point_2_0,&point_3_0,
  &point_0_1,                      &point_3_1,
  &point_0_2,&point_1_2,&point_2_2,&point_3_2,
                                   &point_3_3,
  &point_0_4,&point_1_4,&point_2_4,&point_3_4
};
Point* points_PERIOD[4] = {
 
 
 
  &point_0_3,&point_1_3,
  &point_0_4,&point_1_4
};
Point* points_COMMA[3] = {



  &point_0_3,&point_1_3,
             &point_1_4
};
Point* points_EXCLAMATION[4] = {
  &point_0_0,
  &point_0_1,
  &point_0_2,

  &point_0_4
};
Point* points_QUESTION[6] = {
             &point_1_0,&point_2_0,
  &point_0_1,                      &point_3_1,
                        &point_2_2,

                        &point_2_4
};
Point* points_MINUS[3] = {


             &point_1_2,&point_2_2,&point_3_2


};
Point* points_SLASH[5] = {
                                              &point_4_0,
                                   &point_3_1,
                        &point_2_2,
             &point_1_3,
  &point_0_4
};
Point* points_BACKSLASH[5] = {
  &point_0_0,
             &point_1_1,
                        &point_2_2,
                                   &point_3_3,
                                              &point_4_4
};
Point* points_PLUS[9] = {
                        &point_2_0,
                        &point_2_1,
  &point_0_2,&point_1_2,&point_2_2,&point_3_2,&point_4_2,
                        &point_2_3,
                        &point_2_4
};
Point* points_STAR[5] = {

             &point_1_1,           &point_3_1,
                        &point_2_2,
             &point_1_3,           &point_3_3

};

Point* points_EQUALS[6] = {

             &point_1_1,&point_2_1,&point_3_1,

             &point_1_3,&point_2_3,&point_3_3

};

Point* points_COLON[2] = {

                        &point_2_1,

                        &point_2_3

};

Character char_SPACE(points_NULL, 25);
Character char_NULL(points_NULL, 25);
Character char_A(points_A, 12);
Character char_B(points_B, 13);
Character char_C(points_C, 9);
Character char_D(points_D, 12);
Character char_E(points_E, 10);
Character char_F(points_F, 10);
Character char_G(points_G, 14);
Character char_H(points_H, 12);
Character char_I(points_I, 9);
Character char_J(points_J, 10);
Character char_K(points_K, 10);
Character char_L(points_L, 7);
Character char_M(points_M, 13);
Character char_N(points_N, 13);
Character char_O(points_O, 14);
Character char_P(points_P, 10);
Character char_Q(points_Q, 17);
Character char_R(points_R, 16);
Character char_S(points_S, 14);
Character char_T(points_T, 9);
Character char_U(points_U, 12);
Character char_V(points_V, 9);
Character char_W(points_W, 13);
Character char_X(points_X, 9);
Character char_Y(points_Y, 7);
Character char_Z(points_Z, 13);
Character char_0(points_0, 12);
Character char_1(points_1, 6);
Character char_2(points_2, 10);
Character char_3(points_3, 13);
Character char_4(points_4, 10);
Character char_5(points_5, 12);
Character char_6(points_6, 15);
Character char_7(points_7, 8);
Character char_8(points_8, 14);
Character char_9(points_9, 15);
Character char_PERIOD(points_PERIOD, 4);
Character char_COMMA(points_COMMA, 3);
Character char_EXCLAMATION(points_EXCLAMATION, 4);
Character char_QUESTION(points_QUESTION, 6);
Character char_MINUS(points_MINUS, 3);
Character char_SLASH(points_SLASH, 5);
Character char_BACKSLASH(points_BACKSLASH, 5);
Character char_PLUS(points_PLUS, 9);
Character char_STAR(points_STAR, 5);
Character char_EQUALS(points_EQUALS, 6);
Character char_COLON(points_COLON, 2);

TextProvider::TextProvider()
{
  for (short i = 0; i < 128; i++)
  {
    asciiTable[i] = &char_NULL;
  }
  char_SPACE.setBlank(true,1);
  asciiTable[' '] = &char_SPACE;
  asciiTable['a'] = &char_A;
  asciiTable['b'] = &char_B;
  asciiTable['c'] = &char_C;
  asciiTable['d'] = &char_D;
  asciiTable['e'] = &char_E;
  asciiTable['f'] = &char_F;
  asciiTable['g'] = &char_G;
  asciiTable['h'] = &char_H;
  asciiTable['i'] = &char_I;
  asciiTable['j'] = &char_J;
  asciiTable['k'] = &char_K;
  asciiTable['l'] = &char_L;
  asciiTable['m'] = &char_M;
  asciiTable['n'] = &char_N;
  asciiTable['o'] = &char_O;
  asciiTable['p'] = &char_P;
  asciiTable['q'] = &char_Q;
  asciiTable['r'] = &char_R;
  asciiTable['s'] = &char_S;
  asciiTable['t'] = &char_T;
  asciiTable['u'] = &char_U;
  asciiTable['v'] = &char_V;
  asciiTable['w'] = &char_W;
  asciiTable['x'] = &char_X;
  asciiTable['y'] = &char_Y;
  asciiTable['z'] = &char_Z;
  asciiTable['A'] = &char_A;
  asciiTable['B'] = &char_B;
  asciiTable['C'] = &char_C;
  asciiTable['D'] = &char_D;
  asciiTable['E'] = &char_E;
  asciiTable['F'] = &char_F;
  asciiTable['G'] = &char_G;
  asciiTable['H'] = &char_H;
  asciiTable['I'] = &char_I;
  asciiTable['J'] = &char_J;
  asciiTable['K'] = &char_K;
  asciiTable['L'] = &char_L;
  asciiTable['M'] = &char_M;
  asciiTable['N'] = &char_N;
  asciiTable['O'] = &char_O;
  asciiTable['P'] = &char_P;
  asciiTable['Q'] = &char_Q;
  asciiTable['R'] = &char_R;
  asciiTable['S'] = &char_S;
  asciiTable['T'] = &char_T;
  asciiTable['U'] = &char_U;
  asciiTable['V'] = &char_V;
  asciiTable['W'] = &char_W;
  asciiTable['X'] = &char_X;
  asciiTable['Y'] = &char_Y;
  asciiTable['Z'] = &char_Z;
  asciiTable['0'] = &char_0;
  asciiTable['1'] = &char_1;
  asciiTable['2'] = &char_2;
  asciiTable['3'] = &char_3;
  asciiTable['4'] = &char_4;
  asciiTable['5'] = &char_5;
  asciiTable['6'] = &char_6;
  asciiTable['7'] = &char_7;
  asciiTable['8'] = &char_8;
  asciiTable['9'] = &char_9;
  asciiTable['.'] = &char_PERIOD;
  asciiTable[','] = &char_COMMA;
  asciiTable['!'] = &char_EXCLAMATION;
  asciiTable['?'] = &char_QUESTION;
  asciiTable['-'] = &char_MINUS;
  asciiTable['+'] = &char_PLUS;
  asciiTable['*'] = &char_STAR;
  asciiTable['/'] = &char_SLASH;
  asciiTable['\\'] = &char_BACKSLASH;
  asciiTable['='] = &char_EQUALS;
  asciiTable[':'] = &char_COLON;
}

Character* TextProvider::getDrawingForChar(const char c) const
{
  return asciiTable[c];
}
