#ifndef SETTINGS_H
#define SETTINGS_H

#define MATRIX_COLUMNS 16
#define MATRIX_ROWS 20

#define MASK_A

#ifdef MASK_A

#define PIN 6
#define STRIP_SIZE 241

#endif //MASK_A

#ifdef MASK_B

#define PIN 4
#define STRIP_SIZE 250

#endif //MASK_B

#ifdef MASK_C

#define PIN 4
#define STRIP_SIZE 197

#endif //MASK_C

#include <Adafruit_NeoPixel.h>

class Matrix;
class Adafruit_NeoPixel;

void initializeMatrix(Matrix* pMatrix, Adafruit_NeoPixel* pStrip);



#endif //SETTINGS_H
