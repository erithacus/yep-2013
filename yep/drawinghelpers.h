#ifndef HELPERS_H
#define HELPERS_H

#include "parameters.h"
#include "mathhelpers.h"

class Colour
{
public:
  Colour(short r, short g, short b);
  Colour();
  short getR() const;
  short getG() const;
  short getB() const;
  
  void setR(const short r);
  void setG(const short g);
  void setB(const short b);
  void setRGB(const short r, const short g, const short b);
  
private:
  short m_iR;
  short m_iG;
  short m_iB;
};

class Pixel : public Point, public Colour
{
public:
  Pixel(short x, short y, short r, short g, short b);
};

class Image
{ 
  public: 
    Image(Pixel** pArrayPixels, short iLength);
    short getWidth() const;
    short getHeight() const;
    short getNumberOfPixels() const;
    Pixel* getPixel(short i) const;
    
    
  private:
    Pixel** m_arrayPixels;
    short m_iNumberOfPixels;
    short m_iWidth;
    short m_iHeight;
};

#define DRAWING_DEFAULT 0
#define DRAWING_HEART_FRM1 1
#define DRAWING_HEART_FRM2 2
#define DRAWING_EYE_OPEN 3
#define DRAWING_EYE_CLOSED 4
#define DRAWING_PACMAN_FRM1 5
#define DRAWING_PACMAN_FRM2 6

Image* getDrawing(const short drawing);


#endif //HELPERS_H

