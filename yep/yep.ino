 #include <Adafruit_NeoPixel.h>
#include "matrix.h"
#include "settings.h"
#include "matrixcontroller.h"
#include "textprovider.h"

Matrix matrix = Matrix(MATRIX_COLUMNS,MATRIX_ROWS);
Adafruit_NeoPixel strip = Adafruit_NeoPixel(STRIP_SIZE, PIN, NEO_GRB + NEO_KHZ800);
MatrixController controller;

Colour black(0,0,0);
Colour red(120,0,0);
Colour green(0,120,0);
Colour blue(0,0,120);
Colour yellow(70,70,0);
Colour white(70,70,70);
Colour purple(80,16,120);
Colour orange(120,60,0);

Colour randomColour()
{
  Colour c;
  c.setR(random(120));
  c.setG(random(120));
  c.setB(random(120));
  return c;
}

void setup() 
{
  initializeMatrix(&matrix, &strip);
  controller.setMatrix(&matrix);
  controller.setBackgroundColour(black);
  controller.setDrawingColour(blue);
  randomSeed(analogRead(5));
}

void loop() 
{
  //final YEP sketch
  doRainbow(Point(0,0),controller.getWidthOfMatrix(), controller.getHeightOfMatrix(), 13);
  doTickerBanded("Bru Textiles Year End Party", blue, red, red, 3); 
  doExplodingSquares(black, red, 75, 6);
  doExplodingSquares(black, green, 75, 6);
  doExplodingSquares(black, yellow, 75, 6);
  doExplodingSquares(black, white, 75, 6);
  doExplodingSquares(black, blue, 75, 6);  
  doImplodingSquaresLeft(black, purple, 75, 6);  
  doImplodingSquaresRight(black, orange, 75, 6);  
  doFace(50);                            
  doTickerBanded("Party Animals", red, yellow, yellow, 4);
  doRandomLines(550,50);              
  doEqualizer(3333);
  doStrobo(40, NULL);   
  doTickerBanded("Pintjes zuipen", yellow, green, green, 5); 
  for (int i=0; i<11; i++)
  {
    doFill(&red, 50);
    doFill(&green, 50);
    doFill(&yellow,50);
    doFill(&purple, 50);
    doFill(&orange, 50);
    doFill(&yellow,50);
  }
  doHeartBeat(19);                      
  doRainbow(Point(0,0),controller.getWidthOfMatrix(), controller.getHeightOfMatrix(), 2);
  doHeartBeat(19);
  doRainbow(Point(0,0),controller.getWidthOfMatrix(), controller.getHeightOfMatrix(), 2);
  doHeartBeat(19);
  doRainbow(Point(0,0),controller.getWidthOfMatrix(), controller.getHeightOfMatrix(), 2);    
  doHeartBeat(16);
  doKnightRider(9);
  doRain(10, 2500, red, green, blue);
  doEqualizer(1650);
  doRandomLines(900,50);  
  doStrobo(50, NULL);
  doTickerBanded("Put your hands up in the air!", green, red, blue, 3); 
  for (int i=0; i < 6; i++)
  {
    doExplodingSquares(red, black, 75, 1);
    doExplodingSquares(black, red, 75, 1);
  }
  for (int i=0; i < 6; i++)
  {
    doImplodingSquaresLeft(red, black, 75, 1);
    doImplodingSquaresRight(red, black, 75, 1);
  }
  doEqualizer(3333);
  doRain(10, 2500, white, Colour(0,0,60), blue); 
  for (int i = 0; i < 4; i++)
    doPacman();  
  doRainbow(Point(0,0),controller.getWidthOfMatrix(), controller.getHeightOfMatrix(), 10);
  doFace(30);  
  doSmiley(30000); 
  doTickerBanded("Dansen dansen dansen !", yellow, red, green, 4);    
  for (int i = 0; i < 450; i++)
  {
    short x = random(controller.getWidthOfMatrix());
    short y = random(controller.getHeightOfMatrix());
    controller.setBackgroundColour(black);
    controller.drawSquareFilled(Point(x,y), random(3,8), random(3,8), randomColour());
    controller.show();
    delay(50);
  }
  doStrobo(5, NULL);
  doRain(10, 1600, red, green, blue); 
  doTickerBanded("Copa cabana", blue, yellow,green, 3);
  for (int i = 0; i < 225; i++)
  {
    short x = random(controller.getWidthOfMatrix());
    short y = random(controller.getHeightOfMatrix());
    controller.setBackgroundColour(black);
    controller.drawSquareFilled(Point(x,y), random(3,8), random(3,8), yellow);
    controller.show();
    delay(50);
    controller.setBackgroundColour(black);
    x = random(controller.getWidthOfMatrix());
    y = random(controller.getHeightOfMatrix());
    controller.drawSquareFilled(Point(x,y), random(3,8), random(3,8), red);
    controller.show();
    delay(50);
  }
  for (int i = 0; i < 4; i++)
    doPacman();  
  doKnightRider(9);
  doEqualizer(3000);
  doRandomLines(800,50);
  doFace(30);
  doHeartBeat(19);                      
  doRainbow(Point(0,0),controller.getWidthOfMatrix(), controller.getHeightOfMatrix(), 2);
  doHeartBeat(19);
  doRainbow(Point(0,0),controller.getWidthOfMatrix(), controller.getHeightOfMatrix(), 2);
  doRain(10, 700, red, red, red);
  doExplodingSquares(black, red, 50, 3);
  doExplodingSquares(black, green, 50, 3);
  doExplodingSquares(black, yellow, 50, 3);
  doExplodingSquares(black, white, 50, 3);
  doExplodingSquares(black, blue, 50, 3);  
  doImplodingSquaresLeft(black, purple, 50, 3);  
  doImplodingSquaresRight(black, orange, 50, 3);  
  for (int i = 0; i < 5; i++)
  {
    doExplodingSquares(black, red, 50, 1);
    doExplodingSquares(red, black, 50, 1);
    doExplodingSquares(black, yellow, 50, 1);
    doExplodingSquares(yellow, black, 50, 1);
    doExplodingSquares(blue, red, 50, 1);
    doExplodingSquares(red, black, 50, 1);
    doImplodingSquaresLeft(purple, orange, 50, 1);
    doImplodingSquaresRight(white, red, 50, 1);
  }
  doRainbow(Point(0,0),controller.getWidthOfMatrix(), controller.getHeightOfMatrix(), 2);
  doTickerBanded("Get Lucky...", yellow, blue, blue,3);
  doRain(10, 400, red, green, blue);
  doKnightRider(10);
  doTickerBanded("Get Lucky...", yellow, blue, blue,3);
  doKnightRider(10);
  doRain(10, 400, red, green, blue);
  doTickerBanded("Party On !", blue, red, red, 3);
  controller.setBackgroundColour(black);    
  controller.show();
}

void doEqualizer(unsigned short cycles)
{
  controller.setBackgroundColour(black);
  controller.drawSquareFilled(Point(0,0), controller.getWidthOfMatrix(), 3, Colour(blue));
  controller.drawSquareFilled(Point(0,14), controller.getWidthOfMatrix(), 4, Colour(blue));
  controller.show();
  Point bars[5];
  bars[0].setX(1);
  bars[0].setY(12);
  for (int i=1; i<5; i++)
  {
    bars[i] = bars[i-1].addPoint(3,0);
  }
  short stepChannel = 120 / 8;
  Colour levels[8];
  levels[0].setRGB(0,120,0);
  for (int i=1; i<8; i++)
  {
    levels[i].setR(levels[i-1].getR()+stepChannel);
    levels[i].setG(levels[i-1].getG()-stepChannel);
    levels[i].setB(0);
  }
  short currentLevels[5] = {0,0,0,0,0};
  for (int i = 0; i < cycles; i++)
  {
    for (int b=0; b < 5; b++)
    {
      controller.eraseSquare(bars[b].addPoint(0,-7),1,7);
      if (random(2) == 1)
        currentLevels[b] +=  (random(2) == 1 ? -1 : 1) * random(3);
      if (currentLevels[b] < 0)
        currentLevels[b] = 0;
      if (currentLevels[b] > 7)
        currentLevels[b] = 7;
      switch (currentLevels[b])
      {
        case 7:
          controller.drawPoint(bars[b].addPoint(0,-7), levels[7]);
          controller.drawPoint(bars[b].addPoint(1,-7), levels[7]);
        case 6:
          controller.drawPoint(bars[b].addPoint(0,-6), levels[6]);
          controller.drawPoint(bars[b].addPoint(1,-6), levels[6]);
        case 5:
          controller.drawPoint(bars[b].addPoint(0,-5), levels[5]);
          controller.drawPoint(bars[b].addPoint(1,-5), levels[5]);
        case 4:
          controller.drawPoint(bars[b].addPoint(0,-4), levels[4]);
          controller.drawPoint(bars[b].addPoint(1,-4), levels[4]);
        case 3:
          controller.drawPoint(bars[b].addPoint(0,-3), levels[3]);
          controller.drawPoint(bars[b].addPoint(1,-3), levels[3]);
        case 2:
          controller.drawPoint(bars[b].addPoint(0,-2), levels[2]);
          controller.drawPoint(bars[b].addPoint(1,-2), levels[2]);
        case 1:
          controller.drawPoint(bars[b].addPoint(0,-1), levels[1]);
          controller.drawPoint(bars[b].addPoint(1,-1), levels[1]);
        case 0:
          controller.drawPoint(bars[b].addPoint(0,0), levels[0]);
          controller.drawPoint(bars[b].addPoint(1,0), levels[0]);
        break;
      }
    }
    controller.show();
    delay(5);
  }
}

void doRain(unsigned short wait, unsigned short repeat, Colour cBottom, Colour cMiddle, Colour cTop)
{
  controller.setBackgroundColour(black);
  short speedP = 18;
  short dropP = 10;
  bool isFast[MATRIX_COLUMNS];
  Point drops[MATRIX_COLUMNS];
  for (int i = 0; i < MATRIX_COLUMNS; i++)
  {
    if (random(speedP) == speedP-1)
      isFast[i] = true;
     else isFast[i] = false;
     drops[i] = Point(i,-1);
  }
  for (int r = 0; r < repeat; r++)
  {
     for (int i = 0; i < MATRIX_COLUMNS; i++)
     {
       if (drops[i].getY() == -1)
       { //we're not yet falling...
         if (random(dropP) == dropP-1) 
         {
           drops[i].setY(0); //start falling  
           controller.drawPoint(drops[i],cBottom);
         }
         continue;
       } 
       controller.erasePoint(drops[i]);
       controller.erasePoint(drops[i].addPoint(0,-1));
       controller.erasePoint(drops[i].addPoint(0,-2));
       if (isFast[i])
         drops[i] = drops[i].addPoint(0,1);
       else if (r % 3 == 0)
         drops[i] = drops[i].addPoint(0,1);
       //draw the drop!
       controller.drawPoint(drops[i],cBottom);
       controller.drawPoint(drops[i].addPoint(0,-1),cMiddle);
       controller.drawPoint(drops[i].addPoint(0,-2),cTop);
       if (drops[i].getY() > controller.getHeightOfMatrix() + 3)
       {
         drops[i].setX(i);
         drops[i].setY(-1);
       }
     }
     delay(wait);
     controller.show();
  }
  
}

void doRandomLines(unsigned short repeat, unsigned short wait)
{
  controller.setBackgroundColour(black);
  for(int i=0; i < repeat; i++)
  {
    short y1 = random(controller.getHeightOfMatrix());
    short y2 = random(controller.getHeightOfMatrix());
    Point p1(0,y1);
    Point p2(controller.getWidthOfMatrix(), y2);
    controller.drawLine(p1,p2, randomColour());
    controller.show();
    delay(wait);
    controller.eraseLine(p1,p2);
  }
  
}

void doSmiley(int wait)
{
  controller.setBackgroundColour(yellow);
  Point posM0(3,12);
  Point posM1(5,13);
  Point posM2(7,14);
  Point posM3(9,13);
  Point posM4(11,12);
  controller.setDrawingColour(black);
  controller.drawSquare(posM0,1,1);
  controller.drawSquare(posM1,1,1);
  controller.drawSquare(posM2,1,1);
  controller.drawSquare(posM3,1,1);
  controller.drawSquare(posM4,1,1);
  Point pos1(4,5);
  Point pos2(10,5);
  Point posN(7,9);
  controller.drawSquare(posN,1,1);
  controller.drawSquareFilled(pos1,1,1);
  controller.drawSquareFilled(pos2,1,1);
  controller.show();
  delay(wait);
}

void doPacman()
{
  controller.setBackgroundColour(black);
  short height = controller.getHeightOfMatrix();
  short width = controller.getWidthOfMatrix();
  Image* pPacman1 = getDrawing(DRAWING_PACMAN_FRM1);
  Image* pPacman2 = getDrawing(DRAWING_PACMAN_FRM2);
  Point pos(width, height/2-pPacman1->getHeight()/2);
  while (pos.getX() > 0 - pPacman1->getWidth()-1)
  {
    controller.drawImage(pPacman1, pos);
    controller.show();
    delay(100);
    controller.eraseImage(pPacman1, pos);
    pos.setX(pos.getX()-1);
    controller.drawImage(pPacman2, pos);
    controller.show();
    delay(100);
  }

}

void doRainbow(Point pos, unsigned short width, unsigned short height, short repeats)
{
  controller.setBackgroundColour(black);
  int totalPixels = width * height;
  Point p(0,0);
  Colour drawingColour;
  for (int c=0; c<256*repeats;c++)
  {
    for (int y = pos.getY(); y < pos.getY() + height; y++)
    {
      for (int x = pos.getX(); x < pos.getX() + width; x++)
      {
        int i = x * y;
        uint32_t col = getColourForRainbow(((i * 256 / totalPixels)+c) & 255);
        drawingColour.setR(((col >> 16) & 0xff)/2);
        drawingColour.setG(((col >> 8) & 0xff)/2);
        drawingColour.setB((col & 0xff)/2);
        controller.setDrawingColour(drawingColour);
        p.setX(x);
        p.setY(y);
        controller.drawPoint(p);
      }
    }
    controller.show();
  }
}

uint32_t getColourForRainbow(byte index)
{
  if(index < 85) {
   return strip.Color(index * 3, 255 - index * 3, 0);
  } else if(index < 170) {
   index -= 85;
   return strip.Color(255 - index * 3, 0, index * 3);
  } else {
   index -= 170;
   return strip.Color(0, index * 3, 255 - index * 3);
  }
}

void doExplodingSquares(Colour bg, Colour fg, short wait, unsigned short repeat)
{
  Colour oldBg = controller.getBackgroundColour();
  Colour oldFg = controller.getDrawingColour();
  for (int j = 0; j < repeat; j++)
  {
    Point pos(controller.getWidthOfMatrix() / 2 -1, controller.getHeightOfMatrix() / 2);
    controller.setBackgroundColour(bg);
    controller.setDrawingColour(fg);
    controller.drawSquare(pos, 1, 1);
    controller.show();
    delay(wait);
    short i = 1;
    while (pos.getX() > 0 || pos.getY() > 0)
    {
      pos = pos.addPoint(-1,-1);
      controller.drawSquare(pos, 2+i, 2+i);
      controller.show();
      delay(wait);
      i += 2;
    }
  } 
  controller.setBackgroundColour(oldBg);
  controller.setDrawingColour(oldFg);
}

void doImplodingSquaresLeft(Colour bg, Colour fg, short wait, unsigned short repeat)
{
  Colour oldBg = controller.getBackgroundColour();
  Colour oldFg = controller.getDrawingColour();
  for (int j = 0; j < repeat; j++)
  {
    Point pos(0,0);
    controller.setBackgroundColour(bg);
    controller.setDrawingColour(fg);
    short i = (controller.getWidthOfMatrix() < controller.getHeightOfMatrix()) ? controller.getHeightOfMatrix() : controller.getWidthOfMatrix();
    controller.drawSquare(pos, i, i);
    controller.show();
    delay(wait);
    while (i >= -1 )
    {
      pos = pos.addPoint(1,1);
      controller.drawSquare(pos, 2+i, 2+i);
      controller.show();
      delay(wait);
      i -= 2;
    }
  } 
  controller.setBackgroundColour(oldBg);
  controller.setDrawingColour(oldFg);
}

void doImplodingSquaresRight(Colour bg, Colour fg, short wait, unsigned short repeat)
{
  Colour oldBg = controller.getBackgroundColour();
  Colour oldFg = controller.getDrawingColour();
  for (int j = 0; j < repeat; j++)
  {
    Point pos(controller.getWidthOfMatrix(),0);
    controller.setBackgroundColour(bg);
    controller.setDrawingColour(fg);
    short i = (controller.getWidthOfMatrix() < controller.getHeightOfMatrix()) ? controller.getHeightOfMatrix() : controller.getWidthOfMatrix();
    controller.drawSquare(pos, -i, i);
    controller.show();
    delay(wait);
    while (i >= -1 )
    {
      pos = pos.addPoint(-1,1);
      controller.drawSquare(pos, -(2+i), 2+i);
      controller.show();
      delay(wait);
      i -= 2;
    }
  } 
  controller.setBackgroundColour(oldBg);
  controller.setDrawingColour(oldFg);
}

void doFace(short blinks)
{
  controller.setBackgroundColour(black);
  Colour bg(0,0,0);
  controller.setDrawingColour(green);
  Point pos1(3,5);
  Point pos2(9,5);
  Point posN(7,9);
  controller.drawSquare(posN,1,1);
  Point posM0(3,12);
  Point posM1(5,13);
  Point posM2(7,14);
  Point posM3(9,13);
  Point posM4(11,12);
  controller.setDrawingColour(red);
  controller.drawSquare(posM0,1,1);
  controller.drawSquare(posM1,1,1);
  controller.drawSquare(posM2,1,1);
  controller.drawSquare(posM3,1,1);
  controller.drawSquare(posM4,1,1);
  for (short i=0; i < blinks; i++)
  {  
    controller.drawImage(getDrawing(DRAWING_EYE_OPEN), pos1);
    controller.drawImage(getDrawing(DRAWING_EYE_OPEN), pos2);
    controller.show();
    delay(500);
    controller.drawImage(getDrawing(DRAWING_EYE_CLOSED), pos1);
    controller.drawImage(getDrawing(DRAWING_EYE_CLOSED), pos2);
    controller.show();
    delay(500);
    controller.eraseImage(getDrawing(DRAWING_EYE_CLOSED), pos1);
    controller.eraseImage(getDrawing(DRAWING_EYE_CLOSED), pos2);
  }
}

void doTickerBanded(const char text[], const Colour colourText, const Colour topBandColour, const Colour bottomBandColour, const short iRepeat)
{
  controller.setBackgroundColour(black);
  Point posTop(0,0);
  controller.drawSquareFilled(posTop, controller.getWidthOfMatrix(), 5, topBandColour);
  Point posBottom(0,13);
  controller.drawSquareFilled(posBottom, controller.getWidthOfMatrix(), controller.getHeightOfMatrix()-14, bottomBandColour);
  Colour tempColour = controller.getDrawingColour();
  controller.setDrawingColour(colourText);
  doTicker(text, iRepeat);
  controller.setDrawingColour(tempColour);
}

void doTicker(const char text[], const short iRepeat)
{
  int i = 0;
  Point pos(controller.getWidthOfMatrix()+1,7); 
  Point caret = pos;
  int iToGo=iRepeat;
  while (iToGo > 0)
  {
    controller.drawString(text, pos.addPoint(i, 0));
    controller.show();
    delay(80);
    caret = controller.eraseString(text, pos.addPoint(i, 0));
    if (caret.getX()<0)
    {
      i=0;
      iToGo--;
      continue;
    }
    i--;
  }
}

void doTicker(const char text[], const Colour colour, const short iRepeat)
{
  controller.setBackgroundColour(black);
  Colour tempColour = controller.getDrawingColour();
  controller.setDrawingColour(colour);
  doTicker(text, iRepeat);
  controller.setDrawingColour(tempColour);
}

void doHeartBeat(short repeat)
{
  controller.setBackgroundColour(black);
  short width = controller.getWidthOfMatrix();
  short height = controller.getHeightOfMatrix();
  Image* pFrame_1 = getDrawing(DRAWING_HEART_FRM1);
  Image* pFrame_2 = getDrawing(DRAWING_HEART_FRM2);
  
  Point pos(width/2-pFrame_1->getWidth()/2,height/2-pFrame_1->getHeight()/2);
  for (short i = 0; i < repeat; i++)
  {
    controller.drawImage(pFrame_1,pos);
    controller.show();
    delay(880);    
    controller.drawImage(pFrame_2,pos);
    controller.show();
    delay(120);
  }
}

void doFill(Colour* pCol, short pause)
{
  Colour col = pCol == NULL ? randomColour() : *pCol;
  short height=controller.getHeightOfMatrix();
  short width=controller.getWidthOfMatrix();
  for (int y = height+1; y > 0; y--)
  {
    Point p1(0,y);
    Point p2(width+1,y);
    controller.drawLine(p1,p2,col);
    controller.show();
    delay(pause);
  }
  controller.setBackgroundColour(controller.getBackgroundColour());
}

void doStrobo(short lFlashes, Colour* c)
{
  doStrobo(lFlashes, c, 100, 50);
}

void doStrobo(short lFlashes, Colour* c, short l, short d)
{ 
  Colour oldColour = controller.getBackgroundColour();
  for (int i=0; i<lFlashes; i++)
  {
    controller.setBackgroundColour(c==NULL? randomColour() : *c);
    controller.show();
    delay(l);
    controller.setBackgroundColour(0,0,0);
    controller.show();
    delay(d);
  }
  controller.setBackgroundColour(oldColour);
}

void doKnightRider(unsigned short repeat)
{
  for (int i = 0; i < repeat; i++)
      doKnightRider();
}

void doKnightRider()
{
  short wait = 30;
  short r = 120;
  short r1 = 50;
  short r2 = 20;
  short r3 = 10;
  short r4 = 5;
  matrix.setBackgroundColour(0,0,0);
  matrix.refresh();
  short y = matrix.getDimensionY() / 2 - 2;
  short x;
  for (x = 2; x < matrix.getDimensionX(); x++)
  {
    matrix.setDrawingColour(r1,0,0);
    matrix.drawDot(x,y);
    matrix.drawDot(x,y-1);
    matrix.drawDot(x,y+1);

    matrix.setDrawingColour(r,0,0);
    matrix.drawDot(x-1,y);
    matrix.drawDot(x-1,y-1);
    matrix.drawDot(x-1,y+1);

    matrix.drawDot(x-2,y);
    matrix.drawDot(x-2,y-1);
    matrix.drawDot(x-2,y+1);

    matrix.setDrawingColour(r1,0,0);
    matrix.drawDot(x-3,y);
    matrix.drawDot(x-3,y-1);
    matrix.drawDot(x-3,y+1);
    
    matrix.setDrawingColour(r2,0,0);
    matrix.drawDot(x-4,y);
    matrix.drawDot(x-4,y-1);
    matrix.drawDot(x-4,y+1);
    
    matrix.setDrawingColour(r3,0,0);
    matrix.drawDot(x-5,y);
    matrix.drawDot(x-5,y-1);
    matrix.drawDot(x-5,y+1);
    
    matrix.setDrawingColour(r4,0,0);
    matrix.drawDot(x-6,y);
    matrix.drawDot(x-6,y-1);
    matrix.drawDot(x-6,y+1);
    
    matrix.refresh();
    delay(wait);
    matrix.eraseDot(x,y);
    matrix.eraseDot(x-1,y);
    matrix.eraseDot(x-2,y);
    matrix.eraseDot(x-3,y);
    matrix.eraseDot(x-4,y);
    matrix.eraseDot(x-5,y);
    matrix.eraseDot(x-6,y);
    matrix.eraseDot(x,y-1);
    matrix.eraseDot(x-1,y-1);
    matrix.eraseDot(x-2,y-1);
    matrix.eraseDot(x-3,y-1);
    matrix.eraseDot(x-4,y-1);
    matrix.eraseDot(x-5,y-1);
    matrix.eraseDot(x-6,y-1);
    matrix.eraseDot(x,y+1);
    matrix.eraseDot(x-1,y+1);
    matrix.eraseDot(x-2,y+1);
    matrix.eraseDot(x-3,y+1);
    matrix.eraseDot(x-4,y+1);
    matrix.eraseDot(x-5,y+1);
    matrix.eraseDot(x-6,y+1);
  }
  x--;
  matrix.setDrawingColour(r1,0,0);
  matrix.drawDot(x,y);
  matrix.drawDot(x,y-1);
  matrix.drawDot(x,y+1);

  matrix.setDrawingColour(r,0,0);
  matrix.drawDot(x-1,y);
  matrix.drawDot(x-1,y-1);
  matrix.drawDot(x-1,y+1);

  matrix.drawDot(x-2,y);
  matrix.drawDot(x-2,y-1);
  matrix.drawDot(x-2,y+1);
  
  matrix.setDrawingColour(r1,0,0);
  matrix.drawDot(x-3,y);
  matrix.drawDot(x-3,y-1);
  matrix.drawDot(x-3,y+1);

  matrix.setDrawingColour(r2,0,0);
  matrix.drawDot(x-4,y);
  matrix.drawDot(x-4,y-1);
  matrix.drawDot(x-4,y+1);

  matrix.setDrawingColour(r3,0,0);
  matrix.drawDot(x-5,y);
  matrix.drawDot(x-5,y-1);
  matrix.drawDot(x-5,y+1);
  matrix.refresh();
  delay(wait);
  matrix.eraseDot(x-5,y);
  matrix.eraseDot(x-5,y-1);
  matrix.eraseDot(x-5,y+1);

  matrix.setDrawingColour(r3,0,0);
  matrix.drawDot(x-4,y);
  matrix.drawDot(x-4,y-1);
  matrix.drawDot(x-4,y+1);

  matrix.setDrawingColour(r2,0,0);
  matrix.drawDot(x-3,y);
  matrix.drawDot(x-3,y-1);
  matrix.drawDot(x-3,y+1);

  matrix.refresh();
  delay(wait);  
  matrix.eraseDot(x-4,y);
  matrix.eraseDot(x-4,y-1);
  matrix.eraseDot(x-4,y+1);
  matrix.setDrawingColour(r3,0,0);
  matrix.drawDot(x-3,y);
  matrix.drawDot(x-3,y-1);
  matrix.drawDot(x-3,y+1);
  matrix.refresh();
  delay(wait);  
  matrix.eraseDot(x-3,y);
  matrix.eraseDot(x-3,y-1);
  matrix.eraseDot(x-3,y+1);
  
  matrix.refresh();
  delay(wait);  
  for (x = matrix.getDimensionX()-3; x >= 0; x--)
  {
    matrix.setDrawingColour(r1,0,0);
    matrix.drawDot(x,y);
    matrix.drawDot(x,y-1);
    matrix.drawDot(x,y+1);

    matrix.setDrawingColour(r,0,0);
    matrix.drawDot(x+1,y);
    matrix.drawDot(x+1,y-1);
    matrix.drawDot(x+1,y+1);

    matrix.drawDot(x+2,y);
    matrix.drawDot(x+2,y-1);
    matrix.drawDot(x+2,y+1);
    
    matrix.setDrawingColour(r1,0,0);
    matrix.drawDot(x+3,y);
    matrix.drawDot(x+3,y-1);
    matrix.drawDot(x+3,y+1);
    
    matrix.setDrawingColour(r2,0,0);
    matrix.drawDot(x+4,y);
    matrix.drawDot(x+4,y-1);
    matrix.drawDot(x+4,y+1);

    matrix.setDrawingColour(r3,0,0);
    matrix.drawDot(x+5,y);
    matrix.drawDot(x+5,y-1);
    matrix.drawDot(x+5,y+1);

    matrix.setDrawingColour(r4,0,0);
    matrix.drawDot(x+6,y);
    matrix.drawDot(x+6,y-1);
    matrix.drawDot(x+6,y+1);
    
    matrix.refresh();
    delay(wait);
    matrix.eraseDot(x,y);
    matrix.eraseDot(x+1,y);
    matrix.eraseDot(x+2,y);
    matrix.eraseDot(x+3,y);
    matrix.eraseDot(x+4,y);
    matrix.eraseDot(x+5,y);
    matrix.eraseDot(x+6,y);
    
    matrix.eraseDot(x,y-1);
    matrix.eraseDot(x+1,y-1);
    matrix.eraseDot(x+2,y-1);
    matrix.eraseDot(x+3,y-1);
    matrix.eraseDot(x+4,y-1);
    matrix.eraseDot(x+5,y-1);
    matrix.eraseDot(x+6,y-1);
    
    matrix.eraseDot(x,y+1);
    matrix.eraseDot(x+1,y+1);
    matrix.eraseDot(x+2,y+1);
    matrix.eraseDot(x+3,y+1);
    matrix.eraseDot(x+4,y+1);
    matrix.eraseDot(x+5,y+1);
    matrix.eraseDot(x+6,y+1);
  }
  x++;
  matrix.setDrawingColour(r1,0,0);
  matrix.drawDot(x,y);
  matrix.drawDot(x,y-1);
  matrix.drawDot(x,y+1);

  matrix.setDrawingColour(r,0,0);
  matrix.drawDot(x+1,y);
  matrix.drawDot(x+1,y-1);
  matrix.drawDot(x+1,y+1);

  matrix.drawDot(x+2,y);
  matrix.drawDot(x+2,y-1);
  matrix.drawDot(x+2,y+1);

  matrix.setDrawingColour(r1,0,0);
  matrix.drawDot(x+3,y);
  matrix.drawDot(x+3,y-1);
  matrix.drawDot(x+3,y+1);

  matrix.setDrawingColour(r2,0,0);
  matrix.drawDot(x+4,y);
  matrix.drawDot(x+4,y-1);
  matrix.drawDot(x+4,y+1);

  matrix.setDrawingColour(r3,0,0);
  matrix.drawDot(x+5,y);
  matrix.drawDot(x+5,y-1);
  matrix.drawDot(x+5,y+1);

  matrix.refresh();
  delay(wait);
  matrix.eraseDot(x+5,y);
  matrix.eraseDot(x+5,y-1);
  matrix.eraseDot(x+5,y+1);

  matrix.setDrawingColour(r3,0,0);
  matrix.drawDot(x+4,y);
  matrix.drawDot(x+4,y-1);
  matrix.drawDot(x+4,y+1);

  matrix.setDrawingColour(r2,0,0);
  matrix.drawDot(x+3,y);
  matrix.drawDot(x+3,y-1);
  matrix.drawDot(x+3,y+1);
  matrix.refresh();
  delay(wait);  
  matrix.eraseDot(x+4,y);
  matrix.eraseDot(x+4,y-1);
  matrix.eraseDot(x+4,y+1);

  matrix.setDrawingColour(r3,0,0);
  matrix.drawDot(x+3,y);
  matrix.drawDot(x+3,y-1);
  matrix.drawDot(x+3,y+1);
  matrix.refresh();
  delay(wait);  
  matrix.eraseDot(x+3,y);
  matrix.eraseDot(x+3,y-1);
  matrix.eraseDot(x+3,y+1);
  matrix.refresh();
  delay(wait);    
}

