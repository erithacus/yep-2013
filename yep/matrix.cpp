#include <Adafruit_NeoPixel.h>

#include "matrix.h"


Matrix::Matrix(short x, short y)
{
  //if the size is not acceptable: reset to default values...
  if (!((x > 0) && (x <= MAX_MATRIX_COLUMNS)))
  {
    x = MAX_MATRIX_COLUMNS;
  }
  if (!((y > 0) && (x <= MAX_MATRIX_ROWS)))
  {
    y = MAX_MATRIX_ROWS;
  }
  //set the size (bounding box of our matrix)
  m_sColumns = x;
  m_sRows = y;
  
  //initialize to no addresses for the leds as we have no strip so far
  for (int i = 0; i < m_sColumns; i++)
  {
    for (int j = 0; j < m_sRows; j++)
    {
      m_Matrix[i][j] = -1;
    }
  }
}

Matrix::~Matrix() { /*Nothing to do here for now...*/ }

void Matrix::setStrip(Adafruit_NeoPixel* pStrip)
{
  m_pStrip = pStrip;
}

void Matrix::setStripAddressForCoordinates(short x, short y, short address)
{
  if ((x >= 0) && (x < m_sColumns) && (y >= 0) && (y < m_sRows))
    m_Matrix[x][y] = address;
}

short Matrix::getDimensionX() const
{
  return m_sColumns;
}

short Matrix::getDimensionY() const
{
  return m_sRows;
}

void Matrix::setBackgroundColour(Colour c)
{
  setBackgroundColour(c.getR(), c.getG(), c.getB());
}

void Matrix::setBackgroundColour(short r, short g, short b)
{
  m_cBackgroundColour.setRGB(r,g,b);
  for (int x = 0; x < m_sColumns; x++)
  {
    for (int y = 0; y < m_sRows; y++)
    {
      drawDot(x,y,m_cBackgroundColour);
    }
  }
}

Colour Matrix::getBackgroundColour()
{
  return m_cBackgroundColour;
}

Colour Matrix::getDrawingColour()
{
  return m_cDrawingColour;
}

void Matrix::setDrawingColour(Colour c)
{
  setDrawingColour(c.getR(), c.getG(), c.getB());
}

void Matrix::setDrawingColour(short r, short g, short b)
{
  m_cDrawingColour.setRGB(r,g,b);
}

void Matrix::drawDot(Point p)
{
  drawDot(p.getX(), p.getY());
}

void Matrix::drawDot(short x, short y)
{
  drawDot(x, y, m_cDrawingColour);
}

void Matrix::eraseDot(Point p)
{
  eraseDot(p.getX(), p.getY());
}

void Matrix::eraseDot(short x, short y)
{
  drawDot(x, y, m_cBackgroundColour);
}

void Matrix::drawDot(short x, short y, Colour c)
{
  if ((x >= 0) && (x < m_sColumns) && (y >= 0) && (y < m_sRows))
    if (m_Matrix[x][y] != -1)
      m_pStrip->setPixelColor(m_Matrix[x][y], c.getR(), c.getG(), c.getB());
}

void Matrix::drawLine(short x1, short y1, short x2, short y2, Colour c)
{
  //implement Bresenham 
  short delta_x;
  delta_x = x2 - x1;
  // if x1 == x2, then it does not matter what we set here
  signed char const ix((delta_x > 0) - (delta_x < 0));
  delta_x = abs(delta_x) << 1;
  short delta_y(y2 - y1);
  // if y1 == y2, then it does not matter what we set here
  signed char const iy((delta_y > 0) - (delta_y < 0));
  delta_y = abs(delta_y) << 1;
  drawDot(x1, y1, c);
  if (delta_x >= delta_y)
  {
    // error may go below zero
    short error(delta_y - (delta_x >> 1));
    while (x1 != x2)
    {
      if ((error >= 0) && (error || (ix > 0)))
      {
        error -= delta_x;
        y1 += iy;
      }
      error += delta_y;
      x1 += ix;
      drawDot(x1, y1, c);
    }
  }
  else
  {
    // error may go below zero
    short error(delta_x - (delta_y >> 1));
    while (y1 != y2)
    {
      if ((error >= 0) && (error || (iy > 0)))
      {
        error -= delta_y;
        x1 += ix;
      }
      error += delta_x;
      y1 += iy;
      drawDot(x1, y1, c);
    }
  }
}

void Matrix::drawLine(short x1, short y1, short x2, short y2)
{
  drawLine(x1, y1, x2, y2, m_cDrawingColour);
}

void Matrix::drawLine(Point p1, Point p2)
{
  drawLine(p1.getX(), p1.getY(), p2.getX(), p2.getY());
}

void Matrix::eraseLine(short x1, short y1, short x2, short y2)
{
  drawLine(x1, y1, x2, y2, m_cBackgroundColour);
}

void Matrix::eraseLine(Point p1, Point p2)
{
  eraseLine(p1.getX(), p1.getY(), p2.getX(), p2.getY());
}

void Matrix::drawCircle(short x0, short y0, short radius)
{
  drawCircle(x0, y0, radius, m_cDrawingColour);
}

void Matrix::drawCircle(Point p, short radius)
{
  drawCircle(p.getX(), p.getY(), radius);
}

void Matrix::eraseCircle(short x0, short y0, short radius)
{
  drawCircle(x0, y0, radius, m_cBackgroundColour);
}

void Matrix::eraseCircle(Point p, short radius)
{
  eraseCircle(p.getX(), p.getY(), radius);
}

void Matrix::drawCircle(short x0, short y0, short radius, Colour c) 
{
  int x = 0;
  int y = radius;
  int delta = 2 - 2 * radius;
  int error = 0;
  while(y >= 0) 
  {
    drawDot(x0 + x, y0 + y, c);
    drawDot(x0 + x, y0 - y, c);
    drawDot(x0 - x, y0 + y, c);
    drawDot(x0 - x, y0 - y, c);
    error = 2 * (delta + y) - 1;
    if(delta < 0 && error <= 0) 
    {
      ++x;
      delta += 2 * x + 1;
      continue;
    }
    error = 2 * (delta - x) - 1;
    if(delta > 0 && error > 0) 
    {
      --y;
      delta += 1 - 2 * y;
      continue;
    }
    ++x;
    delta += 2 * (x - y);
    --y;
  }
}

void Matrix::refresh()
{
  m_pStrip->show();
}
