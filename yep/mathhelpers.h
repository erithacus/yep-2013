#ifndef MATHHELPERS_H
#define MATHHELPERS_H

class Point
{
public:
  Point();
  Point(short x, short y);
  short getX() const;
  short getY() const;
  
  short setX(const short x);
  short setY(const short y);
  
  Point addPoint(const Point p) const;
  Point addPoint(const short x, const short y) const;
  
  bool equals(Point p) const;
  
private:
  short m_iX;
  short m_iY;
};

#endif //MATHHELPERS_H

