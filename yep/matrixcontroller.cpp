#include "matrixcontroller.h"
#include "matrix.h"
#include "Arduino.h"

MatrixController::~MatrixController()
{ /*Nothing to do for now*/ }
    
MatrixController::MatrixController()
{ 
  
}

void MatrixController::setMatrix(Matrix* pMatrix)
{ 
  m_pMatrix = pMatrix;
}

short MatrixController::getHeightOfMatrix() const
{
  return m_pMatrix->getDimensionY();
}

short MatrixController::getWidthOfMatrix() const
{
  return m_pMatrix->getDimensionX();
}

void MatrixController::show()
{
  m_pMatrix->refresh();
}

void MatrixController::setBackgroundColour(const Colour c)
{
  setBackgroundColour(c.getR(),c.getG(),c.getB());
}

void MatrixController::setBackgroundColour(const short r, const short g, const short b)
{
  m_pMatrix->setBackgroundColour(r, g, b);
}

void MatrixController::setDrawingColour(const Colour c)
{
  setDrawingColour(c.getR(),c.getG(),c.getB());
}

void MatrixController::setDrawingColour(const short r, const short g, const short b)
{
  m_pMatrix->setDrawingColour(r, g, b);
}

Colour MatrixController::getBackgroundColour() const
{
  return m_pMatrix->getBackgroundColour();
}

Colour MatrixController::getDrawingColour() const
{
  return m_pMatrix->getDrawingColour();
}

Point MatrixController::drawString(const char text[], const Point pos)
{
  int i = 0;
  Point caret = pos;
  while (text[i] != '\0')
  {
    caret = drawCharacter(m_textProvider.getDrawingForChar(text[i]), caret);
    i++;
  }
  return caret;
}

Point MatrixController::drawString(const char text[], const Point pos, const Colour colour)
{
    Colour oldColour = m_pMatrix->getDrawingColour();
    m_pMatrix->setDrawingColour(colour);
    Point pToReturn = drawString(text, pos);
    m_pMatrix->setDrawingColour(oldColour);
    return pToReturn;
}

void MatrixController::drawSquare(const Point pos, const short width, const short height, const Colour colour)
{
  Colour oldColour = m_pMatrix->getDrawingColour();
  m_pMatrix->setDrawingColour(colour);
  drawSquare(pos, width, height);
  m_pMatrix->setDrawingColour(oldColour);  
}

void MatrixController::drawSquare(const Point pos, const short width, const short height)
{
  Point topLeft = pos;
  Point topRight = pos.addPoint(width,0);
  Point bottomLeft = pos.addPoint(0,height);
  Point bottomRight = pos.addPoint(width,height);
  m_pMatrix->drawLine(topLeft, topRight);
  m_pMatrix->drawLine(topLeft, bottomLeft);
  m_pMatrix->drawLine(bottomLeft, bottomRight);
  m_pMatrix->drawLine(bottomRight, topRight);
}

void MatrixController::eraseSquare(const Point pos, const short width, const short height)
{
  drawSquare(pos, width, height, m_pMatrix->getBackgroundColour());
}

void MatrixController::drawSquareFilled(const Point pos, const short width, const short height, const Colour colour)
{
  Colour oldColour = m_pMatrix->getDrawingColour();
  m_pMatrix->setDrawingColour(colour);
  drawSquareFilled(pos, width, height);
  m_pMatrix->setDrawingColour(oldColour);  
}

void MatrixController::drawSquareFilled(const Point pos, const short width, const short height)
{
  Point topLeft = pos;
  Point topRight = pos.addPoint(width,0);
  Point bottomLeft = pos.addPoint(0,height);
  Point bottomRight = pos.addPoint(width,height);
  if (width < 0)
  {
    Point temp = topLeft;
    topLeft = topRight;
    topRight = temp;
    temp = bottomLeft;
    bottomLeft = bottomRight;
    bottomRight = temp;
  }
  if (height < 0)
  {
    Point temp = bottomLeft;
    bottomLeft = topLeft;
    topLeft = temp;
    temp = bottomRight;
    bottomRight = topRight;
    topRight = temp;  
  }
  short newHeight = abs(height);
  short newWidth = abs(width);
  int i=0;
  while ((newHeight > 0) && (newWidth > 0))
  {
    drawSquare(topLeft.addPoint(i,i), newWidth, newHeight);
    i++;
    newWidth--;
    newHeight--;
  }
}

void MatrixController::eraseSquareFilled(const Point pos, const short width, const short height)
{
  drawSquareFilled(pos, width, height, m_pMatrix->getBackgroundColour());
}

Point MatrixController::eraseString(const char text[], const Point pos)
{
  return drawString(text, pos, m_pMatrix->getBackgroundColour());
}

Point MatrixController::drawCharacter(const Character* pCharacter, const Point pos)
{
  if (!pCharacter->isBlank())
  {
    for(int i = 0; i < pCharacter->getNumberOfPoints(); i++)
    {
      Point* p = pCharacter->getPoint(i);
      Point actualPoint = p->addPoint(pos);
      m_pMatrix->drawDot(actualPoint);
    }
  }
  return pos.addPoint(pCharacter->getDimensionX()+2, 0);
}

Point MatrixController::drawCharacter(const Character* pCharacter, const Point pos, const Colour colour)
{
  Colour oldColour = m_pMatrix->getDrawingColour();
  m_pMatrix->setDrawingColour(colour);
  Point pToReturn = drawCharacter(pCharacter, pos);
  m_pMatrix->setDrawingColour(oldColour);
  return pToReturn;
}

Point MatrixController::eraseCharacter(const Character* pCharacter, const Point pos)
{
  return drawCharacter(pCharacter, pos, m_pMatrix->getBackgroundColour());
}

void MatrixController::drawImage(const Image* img, const Point pos)
{
  Colour oldColour = m_pMatrix->getDrawingColour();
  for (int i=0; i < img->getNumberOfPixels(); i++)
  {
    Pixel* p = img->getPixel(i);
    m_pMatrix->setDrawingColour(*p);
    m_pMatrix->drawDot(pos.addPoint(*p));
  }
  m_pMatrix->setDrawingColour(oldColour);
}

void MatrixController::eraseImage(const Image* img, const Point pos)
{
  eraseSquareFilled(pos, img->getWidth(), img->getHeight());
}

void MatrixController::drawPoint(const Point p, const Colour colour)
{
  Colour oldColour = m_pMatrix->getDrawingColour();
  m_pMatrix->setDrawingColour(colour);
  drawPoint(p);
  m_pMatrix->setDrawingColour(oldColour);
}

void MatrixController::drawPoint(const Point p)
{
  m_pMatrix->drawDot(p);
}

void MatrixController::erasePoint(const Point p)
{
  drawPoint(p, m_pMatrix->getBackgroundColour());
}
  
void MatrixController::drawLine(const Point p1, const Point p2, const Colour colour)
{
  Colour oldColour = m_pMatrix->getDrawingColour();
  m_pMatrix->setDrawingColour(colour);
  drawLine(p1, p2);
  m_pMatrix->setDrawingColour(oldColour);
}

void MatrixController::drawLine(const Point p1, const Point p2)
{
  m_pMatrix->drawLine(p1, p2);
}

void MatrixController::eraseLine(const Point p1, const Point p2)
{
  drawLine(p1, p2, m_pMatrix->getBackgroundColour());
}
  
void MatrixController::drawCircle(const Point pos, const short radius, const Colour colour)
{
  Colour oldColour = m_pMatrix->getDrawingColour();
  m_pMatrix->setDrawingColour(colour);
  drawCircle(pos, radius);
  m_pMatrix->setDrawingColour(oldColour);
}

void MatrixController::drawCircle(const Point pos, const short radius)
{
  m_pMatrix->drawCircle(pos, radius);
}

void MatrixController::eraseCircle(const Point pos, const short radius)
{
  drawCircle(pos, radius, m_pMatrix->getBackgroundColour());
}
