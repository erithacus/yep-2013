#ifndef MATRIX_H
#define MATRIX_H

#include "mathhelpers.h"
#include "drawinghelpers.h"

//forward declarations
class Adafruit_NeoPixel;

class Matrix 
{
public:
  Matrix(short x, short y);
  ~Matrix();
  
  void setStrip(Adafruit_NeoPixel* pStrip);
  void setStripAddressForCoordinates(short x, short y, short address);
  short getDimensionX() const;
  short getDimensionY() const;
  
  void setBackgroundColour(Colour c);
  void setBackgroundColour(short r, short g, short b);
  Colour getBackgroundColour();
  void setDrawingColour(Colour c);
  void setDrawingColour(short r, short g, short b);
  Colour getDrawingColour();
  
  void drawDot(Point p);
  void drawDot(short x, short y);
  void eraseDot(Point p);
  void eraseDot(short x, short y);
  
  void drawLine(short x1, short y1, short x2, short y2);
  void drawLine(Point p1, Point p2);
  void eraseLine(short x1, short y1, short x2, short y2);
  void eraseLine(Point p1, Point p2);
  
  void drawCircle(short x0, short y0, short radius);
  void drawCircle(Point p, short radius);
  void eraseCircle(short x0, short y0, short radius);
  void eraseCircle(Point p, short radius);  
  
  void refresh();
  
private:
  void drawDot(short x, short y, Colour c);
  void drawLine(short x1, short y1, short x2, short y2, Colour c);
  void drawCircle(short x0, short y0, short radius, Colour c);
    
private: 
  Colour m_cBackgroundColour;
  Colour m_cDrawingColour;

  short m_sColumns;
  short m_sRows;
  short m_Matrix[MAX_MATRIX_COLUMNS][MAX_MATRIX_ROWS];
  Adafruit_NeoPixel* m_pStrip;
};

#endif //MATRIX_H
